/**************************************************
author:pablo bravo
date:10/09/2013
company:Accedo
mail:pablo.byte@gmail.com
***************************************************/


var feedArray = [];

Ext.onReady(function () {

    $('#div-to-parse-data-hidden').rssfeed('http://rss.cnn.com/services/podcasting/ac360/rss', {
        limit: 6
    },
	function (e) {

	    var elements = Ext.get('div-to-parse-data-hidden').select(".rssRow");

	    Ext.each(elements.elements, function (rssItem) {
	        feedArray.push([
	            rssItem.children[2].textContent, //	            description
	            rssItem.children[1].textContent, //	            pubdate
                getShortNameWithDots(rssItem.children[2].textContent,25) + rssItem.children[1].textContent,//tittle + pubdate
	            rssItem.children[0].children[0].attributes[0].value, //	            tittle
	            rssItem.children[0].children[0].attributes[1].value//	            link

	        ]);
	    });

	    renderPrototype(feedArray);
	});

});

renderPrototype = function (data) {

    // create the data store
    var feedStore = Ext.create('Ext.data.ArrayStore', {
        fields: [
           { name: 'description' },
           { name: 'pubdate' },
           { name: 'renderTittle' },
           { name: 'link' }

        ],
        data: data
    });

    var feedGrid = Ext.create('Ext.grid.Panel', {
        id: 'feedGridID',
        store: feedStore,
        stateId: 'stateGrid',
        hideHeaders: true,
        columns: [
            {
                sortable: false,
                renderer: renderTittle,
                dataIndex: 'renderTittle',
                width: 500
            }
        ],
        height: 380,
        width: 480,
        renderTo: 'div-body-frame-podcast-list',
        viewConfig: {
            stripeRows: true
        },
        listeners: {
            'cellclick': function (grid, td, cellIndex, record, tr, rowIndex) {
                loadFeedVideo(record);
            },
            'cellkeydown': function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
                if (e.keyCode == 13)
                    loadFeedVideo(record);
                else if (e.keyCode == 39)
                    jwplayer().play();
            }

        }
    });
    setKeyboardListerner();

    setFirstTimeValuesAndListeners(feedGrid);
};

renderTittle = function (val) {
    return '<div style="height:50px;"><span class="feed-content-row font-effect-shadow-multiple">' + val + '</span></div>';
};

updateFeedContent = function (record) {
    Ext.fly('span-tittle-podcast').update(Ext.isGecko || Ext.isIE ? record.data.link : record.data.id);
    Ext.fly('span-tittle-description').update(record.data.description);
    Ext.fly('span-currentdescription').update(record.data.description);
};

setKeyboardListerner = function () {
    var charfield = document.getElementById("bodyRssId")
    charfield.onkeydown = function (e) {

        var e = window.event || e;

        if (Ext.getCmp('feedGridID')) {
            var grid = Ext.getCmp('feedGridID');

            switch (e.keyCode) {
                case 40:
                    grid.getSelectionModel().selectNext();
                    break;
                case 38:
                    grid.getSelectionModel().selectPrevious();
                    break;
                case 13:
                    var record = grid.getSelectionModel().getSelection();
                    loadFeedVideo(record);
                    break;
                case 39:
                    if (!Ext.isGecko)
                        jwplayer().play();
                    break;

            }
        }
    }
};

loadFeedVideo = function (record) {
    var currentRecord = record[0] ? record[0] : record;
    var rsslink = Ext.isGecko || Ext.isIE ? currentRecord.data.id : currentRecord.data.link;

    updateFeedContent(currentRecord);

    if (Ext.isGecko) {//FFX doesn't support natively .m4v files
        $("#div-body-frame-podcast-video").empty();
        $("#div-body-frame-podcast-video").append("<iframe src=" + rsslink + "' style='height:430px; width: 460px;'></iframe>");
    }
    else {

        jwplayer("div-body-frame-podcast-video").setup({
            file: rsslink,
            height: 430,
            image: "images/rss.jpg",
            width: 460
        });
    }
}; 

getShortNameWithDots = function (name, maxLenght) {

    if (name.length > maxLenght) {
        return name.substring(0, maxLenght - 3) + '...';
    }

    return name;
};

setFirstTimeValuesAndListeners = function (feedGrid) {

    feedGrid.getSelectionModel().select(0);
    var record = feedGrid.getSelectionModel().getSelection();
    loadFeedVideo(record);

    Ext.get('div-click-up').on('click', function (e) {
        feedGrid.getSelectionModel().selectPrevious();
    });
    Ext.get('div-click-down').on('click', function (e) {
        feedGrid.getSelectionModel().selectNext();
    });
};


