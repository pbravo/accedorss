Here an online Rss app based on Cnn podcasts used to run on desktop devices.
Below the list of features:

- feed list at menu left with keyboard navigation

- video frame at right side with keyboard manipulation

- tittle description tags rendered dynamically 


Keyboard manipulation:
 
 -arrow up: move up into feed list
 -arrow down: move down into feed list
 -enter key: load video and update tag feed information
 -arrow right: start/pause video feed(not supported in Firefox)


technologies used to implement the App:

 -Jwplayer api (used to play video podcast)(not implemented for FFX)
 -Extjs api(used to manipulate dom and render components)
 -Jquery api (used to give support to Jwplayer api)
 -jquery.zrssfeed api(used to parse feeds contained in this app)
 -*html5 cache(used to give offline contents)
 -gloggle css3 styles(implemented on feed list)



* html5 content are supported depending of the current browser version, to see that in action, this app would be hosted in a remote server as well i think.(not all content are cached in this app)
* Cnn podcasts provide video files with .m4v extention, on FFX browser this file extention aparently aren't supported yet, 
  by this reason the behavior on this environment should be a quite diferent showing a download dialog instead.


** TO TEST THAT, JUST OPEN THE MAIN.HTML CONTAINED IN THE FOLDER, THIS WILL LOAD ALL THE FILES THAT THIS APP NEED.


Regards,
Pablo